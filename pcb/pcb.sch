EESchema Schematic File Version 4
LIBS:pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Digital Clock"
Date "2019-08-10"
Rev ""
Comp ""
Comment1 "Bibek Pandey"
Comment2 "Neeraj Adhikari"
Comment3 "Pusparaj Bhattarai"
Comment4 "Safar Sanu Ligal"
$EndDescr
$Comp
L Device:Crystal Y1
U 1 1 5D4E5607
P 1450 7150
F 0 "Y1" V 1450 6950 50  0000 C CNN
F 1 "Crystal" V 1350 6950 50  0000 C CNN
F 2 "Crystal:Crystal_C38-LF_D3.0mm_L8.0mm_Vertical" H 1450 7150 50  0001 C CNN
F 3 "~" H 1450 7150 50  0001 C CNN
	1    1450 7150
	0    1    1    0   
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW3
U 1 1 5D4E6DD8
P 1650 1600
F 0 "SW3" H 1650 1967 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 1650 1876 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 1500 1760 50  0001 C CNN
F 3 "~" H 1650 1860 50  0001 C CNN
	1    1650 1600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5D50036D
P 1550 750
F 0 "SW1" H 1750 850 50  0000 C CNN
F 1 "SW_Push" H 1550 944 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 1550 950 50  0001 C CNN
F 3 "~" H 1550 950 50  0001 C CNN
	1    1550 750 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5D500BA3
P 1550 1100
F 0 "SW2" H 1750 1200 50  0000 C CNN
F 1 "SW_Push" H 1550 1294 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 1550 1300 50  0001 C CNN
F 3 "~" H 1550 1300 50  0001 C CNN
	1    1550 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 5D505F29
P 9850 6250
F 0 "D6" H 9850 6150 50  0000 C CNN
F 1 "LED" H 9843 6375 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 6250 50  0001 C CNN
F 3 "~" H 9850 6250 50  0001 C CNN
	1    9850 6250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D5
U 1 1 5D5072D0
P 9850 5800
F 0 "D5" H 9850 5700 50  0000 C CNN
F 1 "LED" H 9843 5925 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 5800 50  0001 C CNN
F 3 "~" H 9850 5800 50  0001 C CNN
	1    9850 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D4
U 1 1 5D5076CD
P 9850 5350
F 0 "D4" H 9850 5250 50  0000 C CNN
F 1 "LED" H 9843 5475 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 5350 50  0001 C CNN
F 3 "~" H 9850 5350 50  0001 C CNN
	1    9850 5350
	-1   0    0    1   
$EndComp
$Comp
L Timer_RTC:DS1307+ U2
U 1 1 5D50AA71
P 2400 6900
F 0 "U2" H 2750 6450 50  0000 L CNN
F 1 "DS1307+" H 2500 6550 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2400 6400 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/DS1307.pdf" H 2400 6700 50  0001 C CNN
	1    2400 6900
	1    0    0    -1  
$EndComp
$Comp
L MAX7219CNG:MAX7219CNG IC1
U 1 1 5D50C79C
P 6600 1500
F 0 "IC1" H 6550 2470 50  0000 C CNN
F 1 "MAX7219CNG" H 6550 2379 50  0000 C CNN
F 2 "MAX7219CNG:DIL24-3" H 6600 1500 50  0001 L BNN
F 3 "" H 6600 1500 50  0001 L BNN
F 4 "MAX7219CNG" H 6600 1500 50  0001 L BNN "Field4"
F 5 "None" H 6600 1500 50  0001 L BNN "Field5"
F 6 "Maxim Integrated" H 6600 1500 50  0001 L BNN "Field6"
F 7 "Serially Interfaced 8-Digit LED Display Driver IC DIP-24" H 6600 1500 50  0001 L BNN "Field7"
F 8 "Unavailable" H 6600 1500 50  0001 L BNN "Field8"
	1    6600 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 7300 1700 7300
Wire Wire Line
	1700 7300 1700 7100
Wire Wire Line
	1700 7100 1900 7100
$Comp
L power:GND #PWR07
U 1 1 5D529C53
P 2400 7450
F 0 "#PWR07" H 2400 7200 50  0001 C CNN
F 1 "GND" H 2405 7277 50  0000 C CNN
F 2 "" H 2400 7450 50  0001 C CNN
F 3 "" H 2400 7450 50  0001 C CNN
	1    2400 7450
	1    0    0    -1  
$EndComp
Text GLabel 3100 6900 2    50   Input ~ 0
rtc_square_wave
Text GLabel 1650 6700 0    50   Input ~ 0
rtc_serial_clock
Text GLabel 1650 6800 0    50   Input ~ 0
rtc_serial_data
Wire Wire Line
	1450 7000 1900 7000
Wire Wire Line
	1650 6700 1900 6700
Wire Wire Line
	1650 6800 1900 6800
$Comp
L power:GND #PWR03
U 1 1 5D532D4E
P 1750 1100
F 0 "#PWR03" H 1750 850 50  0001 C CNN
F 1 "GND" V 1755 972 50  0000 R CNN
F 2 "" H 1750 1100 50  0001 C CNN
F 3 "" H 1750 1100 50  0001 C CNN
	1    1750 1100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D534E7D
P 1750 750
F 0 "#PWR02" H 1750 500 50  0001 C CNN
F 1 "GND" V 1755 622 50  0000 R CNN
F 2 "" H 1750 750 50  0001 C CNN
F 3 "" H 1750 750 50  0001 C CNN
	1    1750 750 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5D5352C4
P 1350 1600
F 0 "#PWR01" H 1350 1350 50  0001 C CNN
F 1 "GND" V 1355 1472 50  0000 R CNN
F 2 "" H 1350 1600 50  0001 C CNN
F 3 "" H 1350 1600 50  0001 C CNN
	1    1350 1600
	0    1    1    0   
$EndComp
Text GLabel 1150 750  0    50   Input ~ 0
input_mode
Wire Wire Line
	1150 750  1350 750 
Text GLabel 1150 1100 0    50   Input ~ 0
input_change
Wire Wire Line
	1150 1100 1350 1100
Text GLabel 1150 1500 0    50   Input ~ 0
input_rotary_a
Wire Wire Line
	1150 1500 1350 1500
Text GLabel 1150 1700 0    50   Input ~ 0
input_rotary_b
Wire Wire Line
	1150 1700 1350 1700
$Comp
L power:GND #PWR016
U 1 1 5D54B85F
P 10400 850
F 0 "#PWR016" H 10400 600 50  0001 C CNN
F 1 "GND" V 10405 722 50  0000 R CNN
F 2 "" H 10400 850 50  0001 C CNN
F 3 "" H 10400 850 50  0001 C CNN
	1    10400 850 
	0    1    1    0   
$EndComp
Text GLabel 10400 650  0    50   Input ~ 0
beeper_input
Wire Wire Line
	10400 650  10700 650 
$Comp
L Device:LED D3
U 1 1 5D557B78
P 9850 4900
F 0 "D3" H 9850 4800 50  0000 C CNN
F 1 "LED" H 9843 5025 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 4900 50  0001 C CNN
F 3 "~" H 9850 4900 50  0001 C CNN
	1    9850 4900
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 5D557EAA
P 9850 4450
F 0 "D2" H 9850 4350 50  0000 C CNN
F 1 "LED" H 9843 4575 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 4450 50  0001 C CNN
F 3 "~" H 9850 4450 50  0001 C CNN
	1    9850 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 5D558494
P 9850 4000
F 0 "D1" H 9850 3900 50  0000 C CNN
F 1 "LED" H 9843 4125 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9850 4000 50  0001 C CNN
F 3 "~" H 9850 4000 50  0001 C CNN
	1    9850 4000
	-1   0    0    1   
$EndComp
Text GLabel 9350 4000 0    50   Input ~ 0
seven_a
Text GLabel 9350 4450 0    50   Input ~ 0
seven_b
Text GLabel 9350 4900 0    50   Input ~ 0
seven_c
Text GLabel 9350 5350 0    50   Input ~ 0
seven_d
Text GLabel 9350 5800 0    50   Input ~ 0
seven_e
Text GLabel 9350 6250 0    50   Input ~ 0
seven_f
Wire Wire Line
	9350 4000 9700 4000
Wire Wire Line
	9700 4450 9350 4450
Wire Wire Line
	9350 4900 9700 4900
Wire Wire Line
	9700 5350 9350 5350
Wire Wire Line
	9350 5800 9700 5800
Wire Wire Line
	9700 6250 9350 6250
Text GLabel 10350 4000 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 4000 10000 4000
Text GLabel 10350 4450 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 4450 10000 4450
Text GLabel 10350 4900 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 4900 10000 4900
Text GLabel 10350 5350 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 5350 10000 5350
Text GLabel 10350 5800 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 5800 10000 5800
Text GLabel 10350 6250 2    50   Input ~ 0
display_mode
Wire Wire Line
	10350 6250 10000 6250
Text GLabel 7150 2950 0    50   Input ~ 0
seven_a
Text GLabel 7150 3050 0    50   Input ~ 0
seven_b
Text GLabel 7150 3150 0    50   Input ~ 0
seven_c
Text GLabel 7150 3250 0    50   Input ~ 0
seven_d
Text GLabel 7150 3350 0    50   Input ~ 0
seven_e
Text GLabel 7150 3450 0    50   Input ~ 0
seven_f
Text GLabel 7150 3550 0    50   Input ~ 0
seven_g
Wire Wire Line
	7150 2950 7450 2950
Wire Wire Line
	7450 3050 7150 3050
Wire Wire Line
	7150 3150 7450 3150
Wire Wire Line
	7450 3250 7150 3250
Wire Wire Line
	7150 3350 7450 3350
Wire Wire Line
	7450 3450 7150 3450
Wire Wire Line
	7150 3550 7450 3550
Text GLabel 7150 4300 0    50   Input ~ 0
seven_a
Text GLabel 7150 4400 0    50   Input ~ 0
seven_b
Text GLabel 7150 4500 0    50   Input ~ 0
seven_c
Text GLabel 7150 4600 0    50   Input ~ 0
seven_d
Text GLabel 7150 4700 0    50   Input ~ 0
seven_e
Text GLabel 7150 4800 0    50   Input ~ 0
seven_f
Text GLabel 7150 4900 0    50   Input ~ 0
seven_g
Wire Wire Line
	7150 4300 7450 4300
Wire Wire Line
	7450 4400 7150 4400
Wire Wire Line
	7150 4500 7450 4500
Wire Wire Line
	7450 4600 7150 4600
Wire Wire Line
	7150 4700 7450 4700
Wire Wire Line
	7450 4800 7150 4800
Wire Wire Line
	7150 4900 7450 4900
Text GLabel 7150 5650 0    50   Input ~ 0
seven_a
Text GLabel 7150 5750 0    50   Input ~ 0
seven_b
Text GLabel 7150 5850 0    50   Input ~ 0
seven_c
Text GLabel 7150 5950 0    50   Input ~ 0
seven_d
Text GLabel 7150 6050 0    50   Input ~ 0
seven_e
Text GLabel 7150 6150 0    50   Input ~ 0
seven_f
Text GLabel 7150 6250 0    50   Input ~ 0
seven_g
Wire Wire Line
	7150 5650 7450 5650
Wire Wire Line
	7450 5750 7150 5750
Wire Wire Line
	7150 5850 7450 5850
Wire Wire Line
	7450 5950 7150 5950
Wire Wire Line
	7150 6050 7450 6050
Wire Wire Line
	7450 6150 7150 6150
Wire Wire Line
	7150 6250 7450 6250
Text GLabel 6000 2950 2    50   Input ~ 0
seven_a
Text GLabel 6000 3050 2    50   Input ~ 0
seven_b
Text GLabel 6000 3150 2    50   Input ~ 0
seven_c
Text GLabel 6000 3250 2    50   Input ~ 0
seven_d
Text GLabel 6000 3350 2    50   Input ~ 0
seven_e
Text GLabel 6000 3450 2    50   Input ~ 0
seven_f
Text GLabel 6000 3550 2    50   Input ~ 0
seven_g
Wire Wire Line
	6000 2950 5700 2950
Wire Wire Line
	5700 3050 6000 3050
Wire Wire Line
	6000 3150 5700 3150
Wire Wire Line
	5700 3250 6000 3250
Wire Wire Line
	6000 3350 5700 3350
Wire Wire Line
	5700 3450 6000 3450
Wire Wire Line
	6000 3550 5700 3550
Text GLabel 6000 4300 2    50   Input ~ 0
seven_a
Text GLabel 6000 4400 2    50   Input ~ 0
seven_b
Text GLabel 6000 4500 2    50   Input ~ 0
seven_c
Text GLabel 6000 4600 2    50   Input ~ 0
seven_d
Text GLabel 6000 4700 2    50   Input ~ 0
seven_e
Text GLabel 6000 4800 2    50   Input ~ 0
seven_f
Text GLabel 6000 4900 2    50   Input ~ 0
seven_g
Wire Wire Line
	6000 4300 5700 4300
Wire Wire Line
	5700 4400 6000 4400
Wire Wire Line
	6000 4500 5700 4500
Wire Wire Line
	5700 4600 6000 4600
Wire Wire Line
	6000 4700 5700 4700
Wire Wire Line
	5700 4800 6000 4800
Wire Wire Line
	6000 4900 5700 4900
Text GLabel 6000 5650 2    50   Input ~ 0
seven_a
Text GLabel 6000 5750 2    50   Input ~ 0
seven_b
Text GLabel 6000 5850 2    50   Input ~ 0
seven_c
Text GLabel 6000 5950 2    50   Input ~ 0
seven_d
Text GLabel 6000 6050 2    50   Input ~ 0
seven_e
Text GLabel 6000 6150 2    50   Input ~ 0
seven_f
Text GLabel 6000 6250 2    50   Input ~ 0
seven_g
Wire Wire Line
	6000 5650 5700 5650
Wire Wire Line
	5700 5750 6000 5750
Wire Wire Line
	6000 5850 5700 5850
Wire Wire Line
	5700 5950 6000 5950
Wire Wire Line
	6000 6050 5700 6050
Wire Wire Line
	5700 6150 6000 6150
Wire Wire Line
	6000 6250 5700 6250
Text GLabel 8450 3550 2    50   Input ~ 0
display_2
Wire Wire Line
	8450 3550 8050 3550
Text GLabel 8450 4900 2    50   Input ~ 0
display_4
Wire Wire Line
	8450 4900 8050 4900
Text GLabel 8450 6250 2    50   Input ~ 0
display_6
Wire Wire Line
	8450 6250 8050 6250
Text GLabel 4700 3550 0    50   Input ~ 0
display_1
Wire Wire Line
	4700 3550 5100 3550
Text GLabel 4700 4900 0    50   Input ~ 0
display_3
Wire Wire Line
	4700 4900 5100 4900
Text GLabel 4700 6250 0    50   Input ~ 0
display_5
Wire Wire Line
	4700 6250 5100 6250
Text GLabel 5700 800  0    50   Input ~ 0
display_1
Wire Wire Line
	5700 800  6100 800 
Text GLabel 5700 1000 0    50   Input ~ 0
display_3
Wire Wire Line
	5700 1000 6100 1000
Text GLabel 5700 1200 0    50   Input ~ 0
display_5
Wire Wire Line
	5700 1200 6100 1200
Text GLabel 5700 900  0    50   Input ~ 0
display_2
Wire Wire Line
	5700 900  6100 900 
Text GLabel 5700 1100 0    50   Input ~ 0
display_4
Wire Wire Line
	5700 1100 6100 1100
Text GLabel 5700 1300 0    50   Input ~ 0
display_6
Wire Wire Line
	5700 1300 6100 1300
Text GLabel 5700 1400 0    50   Input ~ 0
display_mode
Wire Wire Line
	5700 1400 6100 1400
Wire Wire Line
	5700 1600 6100 1600
Wire Wire Line
	6100 1700 5700 1700
Wire Wire Line
	5700 1800 6100 1800
Wire Wire Line
	6100 1900 5700 1900
Wire Wire Line
	5700 2000 6100 2000
Wire Wire Line
	6100 2100 5700 2100
Wire Wire Line
	5700 2200 6100 2200
Text GLabel 5700 2200 0    50   Input ~ 0
seven_g
Text GLabel 5700 2100 0    50   Input ~ 0
seven_f
Text GLabel 5700 2000 0    50   Input ~ 0
seven_e
Text GLabel 5700 1900 0    50   Input ~ 0
seven_d
Text GLabel 5700 1800 0    50   Input ~ 0
seven_c
Text GLabel 5700 1700 0    50   Input ~ 0
seven_b
Text GLabel 5700 1600 0    50   Input ~ 0
seven_a
NoConn ~ 6100 1500
NoConn ~ 7200 1300
NoConn ~ 1950 1700
NoConn ~ 1950 1500
NoConn ~ 5100 3650
NoConn ~ 5700 3650
NoConn ~ 7450 3650
NoConn ~ 8050 3650
NoConn ~ 8050 5000
NoConn ~ 8050 6350
NoConn ~ 7450 6350
NoConn ~ 7450 5000
NoConn ~ 5700 5000
NoConn ~ 5700 6350
NoConn ~ 5100 6350
NoConn ~ 5100 5000
NoConn ~ 6100 2300
Text GLabel 7650 800  2    50   Input ~ 0
max_clock
Wire Wire Line
	7650 800  7200 800 
Text GLabel 7650 1000 2    50   Input ~ 0
max_load
Wire Wire Line
	7650 1000 7200 1000
Text GLabel 7650 1200 2    50   Input ~ 0
max_din
Wire Wire Line
	7650 1200 7200 1200
Wire Wire Line
	7600 1500 7450 1500
$Comp
L Device:R R1
U 1 1 5D6ABF8A
P 7750 1500
F 0 "R1" V 7850 1450 50  0000 L CNN
F 1 "R" V 7650 1500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 7680 1500 50  0001 C CNN
F 3 "~" H 7750 1500 50  0001 C CNN
	1    7750 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 7450 2400 7300
Wire Wire Line
	2900 6900 3100 6900
$Comp
L Device:Buzzer BZ1
U 1 1 5D8B1997
P 10800 750
F 0 "BZ1" H 10952 779 50  0000 L CNN
F 1 "Buzzer" H 10952 688 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" V 10775 850 50  0001 C CNN
F 3 "~" V 10775 850 50  0001 C CNN
	1    10800 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 850  10700 850 
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5D8DA03D
P 7450 1650
F 0 "#FLG02" H 7450 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 7450 1823 50  0000 C CNN
F 2 "" H 7450 1650 50  0001 C CNN
F 3 "~" H 7450 1650 50  0001 C CNN
	1    7450 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 1650 7450 1500
Connection ~ 7450 1500
Wire Wire Line
	7450 1500 7200 1500
$Comp
L power:VCC #PWR010
U 1 1 5D9462CA
P 7600 2100
F 0 "#PWR010" H 7600 1950 50  0001 C CNN
F 1 "VCC" V 7618 2228 50  0000 L CNN
F 2 "" H 7600 2100 50  0001 C CNN
F 3 "" H 7600 2100 50  0001 C CNN
	1    7600 2100
	0    1    -1   0   
$EndComp
Wire Wire Line
	7600 2100 7200 2100
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5D97E1BC
P 10850 1400
F 0 "J1" H 10930 1392 50  0000 L CNN
F 1 "Conn_01x02" H 10930 1301 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10850 1400 50  0001 C CNN
F 3 "~" H 10850 1400 50  0001 C CNN
	1    10850 1400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR014
U 1 1 5D97FA5A
P 10250 1400
F 0 "#PWR014" H 10250 1250 50  0001 C CNN
F 1 "VCC" V 10268 1528 50  0000 L CNN
F 2 "" H 10250 1400 50  0001 C CNN
F 3 "" H 10250 1400 50  0001 C CNN
	1    10250 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 1400 10450 1400
$Comp
L power:GND #PWR015
U 1 1 5D987987
P 10250 1500
F 0 "#PWR015" H 10250 1250 50  0001 C CNN
F 1 "GND" V 10255 1372 50  0000 R CNN
F 2 "" H 10250 1500 50  0001 C CNN
F 3 "" H 10250 1500 50  0001 C CNN
	1    10250 1500
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5D9A3950
P 10450 1250
F 0 "#FLG03" H 10450 1325 50  0001 C CNN
F 1 "PWR_FLAG" H 10450 1423 50  0000 C CNN
F 2 "" H 10450 1250 50  0001 C CNN
F 3 "~" H 10450 1250 50  0001 C CNN
	1    10450 1250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10450 1250 10450 1400
Connection ~ 10450 1400
Wire Wire Line
	10450 1400 10650 1400
$Comp
L power:GND #PWR011
U 1 1 5D9B28E1
P 7600 2200
F 0 "#PWR011" H 7600 1950 50  0001 C CNN
F 1 "GND" V 7605 2072 50  0000 R CNN
F 2 "" H 7600 2200 50  0001 C CNN
F 3 "" H 7600 2200 50  0001 C CNN
	1    7600 2200
	0    -1   1    0   
$EndComp
Wire Wire Line
	7600 2200 7200 2200
$Comp
L power:GND #PWR012
U 1 1 5D9BB3E9
P 7600 2300
F 0 "#PWR012" H 7600 2050 50  0001 C CNN
F 1 "GND" V 7605 2172 50  0000 R CNN
F 2 "" H 7600 2300 50  0001 C CNN
F 3 "" H 7600 2300 50  0001 C CNN
	1    7600 2300
	0    -1   1    0   
$EndComp
Wire Wire Line
	7600 2300 7200 2300
$Comp
L power:VCC #PWR013
U 1 1 5D9CA538
P 8250 1500
F 0 "#PWR013" H 8250 1350 50  0001 C CNN
F 1 "VCC" V 8268 1628 50  0000 L CNN
F 2 "" H 8250 1500 50  0001 C CNN
F 3 "" H 8250 1500 50  0001 C CNN
	1    8250 1500
	0    1    -1   0   
$EndComp
Wire Wire Line
	7900 1500 8250 1500
$Comp
L power:VCC #PWR06
U 1 1 5DA0C758
P 2300 6200
F 0 "#PWR06" H 2300 6050 50  0001 C CNN
F 1 "VCC" H 2318 6373 50  0000 C CNN
F 2 "" H 2300 6200 50  0001 C CNN
F 3 "" H 2300 6200 50  0001 C CNN
	1    2300 6200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 6200 2300 6500
$Comp
L BS-7:BS-7 BH1
U 1 1 5D4E8C61
P 3200 6050
F 0 "BH1" V 3591 6000 50  0000 C CNN
F 1 "BS-7" V 3500 6000 50  0000 C CNN
F 2 "BS-7:BS-7" H 3200 6050 50  0001 L BNN
F 3 "" H 3200 6050 50  0001 L BNN
F 4 "BS-7" H 3200 6050 50  0001 L BNN "Field4"
F 5 "None" H 3200 6050 50  0001 L BNN "Field5"
F 6 "MPD" H 3200 6050 50  0001 L BNN "Field6"
F 7 "CR2032 holder, battery rel" H 3200 6050 50  0001 L BNN "Field7"
F 8 "Unavailable" H 3200 6050 50  0001 L BNN "Field8"
	1    3200 6050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 6500 2400 6050
Wire Wire Line
	2400 6050 2650 6050
$Comp
L power:GND #PWR09
U 1 1 5D50734B
P 3950 6300
F 0 "#PWR09" H 3950 6050 50  0001 C CNN
F 1 "GND" H 3955 6127 50  0000 C CNN
F 2 "" H 3950 6300 50  0001 C CNN
F 3 "" H 3950 6300 50  0001 C CNN
	1    3950 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 2250 1750 2450
$Comp
L power:VCC #PWR04
U 1 1 5DA308A5
P 1750 2250
F 0 "#PWR04" H 1750 2100 50  0001 C CNN
F 1 "VCC" H 1768 2423 50  0000 C CNN
F 2 "" H 1750 2250 50  0001 C CNN
F 3 "" H 1750 2250 50  0001 C CNN
	1    1750 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2800 4400 2350 4400
$Comp
L power:VCC #PWR08
U 1 1 5D9EF4E9
P 2800 4400
F 0 "#PWR08" H 2800 4250 50  0001 C CNN
F 1 "VCC" V 2818 4528 50  0000 L CNN
F 2 "" H 2800 4400 50  0001 C CNN
F 3 "" H 2800 4400 50  0001 C CNN
	1    2800 4400
	0    1    -1   0   
$EndComp
Wire Wire Line
	1750 5800 1750 5650
$Comp
L MCU_Microchip_ATmega:ATmega328P-PU U1
U 1 1 5D4E9FF9
P 1750 4100
F 0 "U1" H 1200 3550 50  0000 R CNN
F 1 "ATmega328P-PU" V 1150 3400 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 1750 4100 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 1750 4100 50  0001 C CNN
	1    1750 4100
	1    0    0    -1  
$EndComp
NoConn ~ 1150 2900
NoConn ~ 1850 2600
Wire Wire Line
	2350 3600 2800 3600
Text GLabel 2800 3600 2    50   Input ~ 0
rtc_square_wave
Wire Wire Line
	2800 4200 2350 4200
Wire Wire Line
	2800 4300 2350 4300
Text GLabel 2800 4200 2    50   Input ~ 0
rtc_serial_data
Text GLabel 2800 4300 2    50   Input ~ 0
rtc_serial_clock
Wire Wire Line
	2800 4100 2350 4100
Text GLabel 2800 4100 2    50   Input ~ 0
input_rotary_b
Wire Wire Line
	2800 4000 2350 4000
Text GLabel 2800 4000 2    50   Input ~ 0
input_rotary_a
Wire Wire Line
	2800 3900 2350 3900
Text GLabel 2800 3900 2    50   Input ~ 0
input_change
Wire Wire Line
	2800 3800 2350 3800
Text GLabel 2800 3800 2    50   Input ~ 0
input_mode
Wire Wire Line
	2800 3500 2350 3500
Text GLabel 2800 3500 2    50   Input ~ 0
beeper_input
Wire Wire Line
	2800 3400 2350 3400
Text GLabel 2800 3400 2    50   Input ~ 0
max_clock
Wire Wire Line
	2800 3100 2350 3100
Text GLabel 2800 3100 2    50   Input ~ 0
max_load
Wire Wire Line
	2800 3200 2350 3200
Text GLabel 2800 3200 2    50   Input ~ 0
max_din
NoConn ~ 2350 3300
NoConn ~ 2350 3000
NoConn ~ 2350 2900
NoConn ~ 2350 5300
NoConn ~ 2350 5200
NoConn ~ 2350 5100
NoConn ~ 2350 5000
NoConn ~ 2350 4900
NoConn ~ 2350 4800
NoConn ~ 2350 4700
NoConn ~ 2350 4600
$Comp
L power:GND #PWR05
U 1 1 5D6FB757
P 1750 5800
F 0 "#PWR05" H 1750 5550 50  0001 C CNN
F 1 "GND" H 1755 5627 50  0000 C CNN
F 2 "" H 1750 5800 50  0001 C CNN
F 3 "" H 1750 5800 50  0001 C CNN
	1    1750 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D5331C2
P 700 3850
F 0 "C1" H 815 3896 50  0000 L CNN
F 1 "C" H 815 3805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 738 3700 50  0001 C CNN
F 3 "~" H 700 3850 50  0001 C CNN
	1    700  3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  3700 700  2450
Wire Wire Line
	700  2450 1750 2450
Connection ~ 1750 2450
Wire Wire Line
	1750 2450 1750 2600
Wire Wire Line
	700  4000 700  5650
Wire Wire Line
	700  5650 1750 5650
Connection ~ 1750 5650
Wire Wire Line
	1750 5650 1750 5600
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5D54C8F7
P 2650 5750
F 0 "#FLG01" H 2650 5825 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 5923 50  0000 C CNN
F 2 "" H 2650 5750 50  0001 C CNN
F 3 "~" H 2650 5750 50  0001 C CNN
	1    2650 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 5750 2650 6050
Connection ~ 2650 6050
Wire Wire Line
	2650 6050 2700 6050
Wire Wire Line
	3950 6300 3950 6050
Wire Wire Line
	3950 6050 3800 6050
Wire Wire Line
	10250 1500 10650 1500
$Comp
L Display_Character:KCSC02-105 U5
U 1 1 5D58C903
P 5400 5950
F 0 "U5" H 5400 6617 50  0000 C CNN
F 1 "KCSC02-105" H 5400 6526 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 5400 5350 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 4900 6425 50  0001 L CNN
	1    5400 5950
	-1   0    0    -1  
$EndComp
$Comp
L Display_Character:KCSC02-105 U6
U 1 1 5D58E28A
P 7750 3250
F 0 "U6" H 7750 3917 50  0000 C CNN
F 1 "KCSC02-105" H 7750 3826 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 7750 2650 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 7250 3725 50  0001 L CNN
	1    7750 3250
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:KCSC02-105 U7
U 1 1 5D58F935
P 7750 4600
F 0 "U7" H 7750 5267 50  0000 C CNN
F 1 "KCSC02-105" H 7750 5176 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 7750 4000 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 7250 5075 50  0001 L CNN
	1    7750 4600
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:KCSC02-105 U8
U 1 1 5D591134
P 7750 5950
F 0 "U8" H 7750 6617 50  0000 C CNN
F 1 "KCSC02-105" H 7750 6526 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 7750 5350 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 7250 6425 50  0001 L CNN
	1    7750 5950
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:KCSC02-105 U3
U 1 1 5D59A474
P 5400 3250
F 0 "U3" H 5400 3917 50  0000 C CNN
F 1 "KCSC02-105" H 5400 3826 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 5400 2650 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 4900 3725 50  0001 L CNN
	1    5400 3250
	-1   0    0    -1  
$EndComp
$Comp
L Display_Character:KCSC02-105 U4
U 1 1 5D59B23B
P 5400 4600
F 0 "U4" H 5400 5267 50  0000 C CNN
F 1 "KCSC02-105" H 5400 5176 50  0000 C CNN
F 2 "Display_7Segment:7SegmentLED_LTS6760_LTS6780" H 5400 4000 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSC02-105(Ver.9A).pdf" H 4900 5075 50  0001 L CNN
	1    5400 4600
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
